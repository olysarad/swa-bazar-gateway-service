package cz.cvut.fel.swa.swabazar.gatewayservice.entites.auth;

import io.swagger.v3.oas.annotations.media.Schema;

public class UserCredentials {
    @Schema(example= "Pavlik")
    public String username;
    @Schema(example= "password")
    public String password;
}
