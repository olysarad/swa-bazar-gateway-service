package cz.cvut.fel.swa.swabazar.gatewayservice.entites.user;

import io.swagger.v3.oas.annotations.media.Schema;

public class User {
    @Schema(example= "22", description = "Age of user")
    public Integer age;
    @Schema(example= "1999-11-08", description = "Date of birth in format yyyy-mm-dd")
    public String dateOfBirth;
    @Schema(example= "pavel@domain.com", description = "Email of user")
    public String email;
    @Schema(example= "Pavel Slavík", description = "Full name of user")
    public String fullName;
    @Schema(example= "11", description = "USer id")
    public Integer id;
    @Schema(example= "Pavlik007", description = "Username of user")
    public String username;
}
