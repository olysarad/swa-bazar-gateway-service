package cz.cvut.fel.swa.swabazar.gatewayservice.controllers;

import com.google.gson.Gson;
import cz.cvut.fel.swa.swabazar.gatewayservice.entites.auth.UserCredentials;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.*;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.URLEncoder;


@RestController
@RequestMapping(path = "/v1/auth")
public class LoginServiceController {

    @Value("${login.service.path}")
    private String apiPath;

    // USER REGISTRATION
    @Operation(summary = "Register user", tags = "Auth")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", content = @Content(schema = @Schema(hidden = true)), description = "Success"),
            @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(hidden = true)), description = "Returns error message and possibly data object with fields of incorrect values"),
            @ApiResponse(responseCode = "422", content = @Content(schema = @Schema(hidden = true)), description = "Duplicate values"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")}
    )
    @RequestMapping(path = "/register", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity register(@RequestBody UserCredentials userCredentials) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(apiPath+"/authorization/register");
            StringEntity params = new StringEntity(new Gson().toJson(userCredentials));
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // USER LOGIN
    @Operation(summary = "Login user", tags = "Auth")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Success"),
            @ApiResponse(responseCode = "422", content = @Content(schema = @Schema(hidden = true)), description = "Errors"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")}
    )
    @RequestMapping(path = "/login", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity login(@RequestBody UserCredentials userCredentials) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(apiPath+"/authorization/login");
            StringEntity params = new StringEntity(new Gson().toJson(userCredentials));
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // DELETE USER FROM LOGIN SERVICE
    public ResponseEntity deleteUserFromLogin(String token) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpDelete request = new HttpDelete(apiPath+"/authorization/"+token.substring("Bearer ".length()));
            request.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(request);
            return ResponseEntity.status(response.getStatusLine().getStatusCode()).build();
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // GET USERID FROM TOKEN
    public Long getUserId(String token) {
        if(!token.startsWith("Bearer ")){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid token - does not start with Bearer ");
        }
        token = token.substring("Bearer ".length());

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = null;
        try {
            HttpGet request = new HttpGet(apiPath+"/authorization/"+ URLEncoder.encode(token));
            request.addHeader("content-type", "application/json");
            response = httpClient.execute(request);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Token was not found");
        }
        HttpEntity entity = response.getEntity();
        try {
            return Long.parseLong(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }
}
