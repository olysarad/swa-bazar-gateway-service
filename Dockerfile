FROM adoptopenjdk/openjdk11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} /usr/app/app.jar
COPY src/main/resources/application-docker.properties /config/application-docker.properties
ENTRYPOINT ["java","-jar","/usr/app/app.jar", "--spring.config.location=file:/config/application-docker.properties"]
