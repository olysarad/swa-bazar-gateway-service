package cz.cvut.fel.swa.swabazar.gatewayservice.controllers;

import com.google.gson.Gson;
import cz.cvut.fel.swa.swabazar.gatewayservice.entites.messages.Message;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(path = "/v1/messages")
@SecurityScheme(
        name = "bearerAuth",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class MessageServiceController {
    @Autowired
    LoginServiceController loginServiceController;

    @Value("${message.service.path}")
    private String apiPath;

    // GET ALL USER MESSAGES
    @Operation(summary = "Get list of all user messages (sent or received)", tags = "Messages", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Success"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")
    })
    @GetMapping(path = "", produces = "application/json")
    public ResponseEntity getMyMessages(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token) {
        Long userId = loginServiceController.getUserId(token);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpGet request = new HttpGet(apiPath+"/api/v1/messages/user/"+userId);
            request.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // GET ALL MESSAGES FROM USER
    @Operation(summary = "Get list of all user messages sent from user with given id", tags = "Messages", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Success"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")
    })
    @GetMapping(path = "/from/{fromUserId}", produces = "application/json")
    public ResponseEntity getMyMessagesFromUser(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token, @PathVariable("fromUserId") Long fromUserId) {
        Long toUserId = loginServiceController.getUserId(token);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpGet request = new HttpGet(apiPath+"/api/v1/messages/from/" + fromUserId + "/to/"+ toUserId);
            request.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // GET ALL MESSAGES TO USER
    @Operation(summary = "Get list of all user messages sent to user with given id", tags = "Messages", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Success"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")
    })
    @GetMapping(path = "/to/{toUserId}", produces = "application/json")
    public ResponseEntity getMyMessagesToUser(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token, @PathVariable("toUserId") Long toUserId) {
        Long fromUserId = loginServiceController.getUserId(token);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpGet request = new HttpGet(apiPath+"/api/v1/messages/from/" + fromUserId + "/to/"+ toUserId);
            request.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }


    // SEND MESSAGE (HOW TO GET PARAMETERS!!!)
    @Operation(summary = "Send message", tags = "Messages", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Success"),
            @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(hidden = true)), description = "Bad request"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")}
    )
    @RequestMapping(path = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity sendMessage(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token, @RequestBody Message msg) {
        msg.fromUserId = loginServiceController.getUserId(token);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(apiPath+"/api/v1/messages");
            StringEntity params = new StringEntity(new Gson().toJson(msg));
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }
}
