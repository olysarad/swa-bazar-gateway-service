package cz.cvut.fel.swa.swabazar.gatewayservice.entites.bazar;

import io.swagger.v3.oas.annotations.media.Schema;

public class Category {
    public Long id;
    public String name;
}
