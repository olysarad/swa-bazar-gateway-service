package cz.cvut.fel.swa.swabazar.gatewayservice.controllers;

import com.google.gson.Gson;
import cz.cvut.fel.swa.swabazar.gatewayservice.entites.bazar.Item;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.json.JsonPatch;

@RestController
@RequestMapping(path = "/v1/bazar")
@SecurityScheme(
        name = "bearerAuth",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class BazarServiceController {
    @Autowired
    LoginServiceController loginServiceController;

    @Value("${bazar.service.path}")
    private String apiPath;

    // GET ITEM WITH SPECIFIED ID
    @Operation(summary = "Get item with specific id", tags = "Bazar items", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Item successfully found"),
            @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(hidden = true)), description = "Not found - The item was not found"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")
    })
    @GetMapping(path = "/items/{itemId}", produces = "application/json")
    public ResponseEntity getItem(@PathVariable("itemId") Long itemId, @RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpGet request = new HttpGet(apiPath+"/api/items/"+itemId);
            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", token);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // UPDATE ITEM
    @Operation(summary = "Update item values", tags = "Bazar items", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Item has been successfully updated"),
            @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(hidden = true)), description = "Request body has invalid format"),
            @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(hidden = true)), description = "Item not found"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")

    })
    @PatchMapping(path = "/items/{itemId}", produces = "application/json")
    @Parameters({
            @Parameter(name = "itemId", required = true),
            @Parameter(
                    name = "patchDocument",
                    example = "[{ \"op\": \"replace\", \"path\": \"/name\", \"value\": \"New name\" }]"
            )
    })
    public ResponseEntity updateItem(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token, @PathVariable("itemId") Long itemId, @RequestBody String patchDocument) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPatch request = new HttpPatch(apiPath+"/api/items/"+itemId);
            StringEntity params = new StringEntity(patchDocument);
            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", token);
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            return ResponseEntity.status(response.getStatusLine().getStatusCode()).build();
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // DELETE ITEM
    @Operation(summary = "Delete item", tags = "Bazar items", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Item successfully deleted"),
            @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(hidden = true)), description = "Not found - The item was not found"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")

    })
    @DeleteMapping(path = "/items/{itemId}", produces = "application/json")
    public ResponseEntity deleteItem(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token,  @PathVariable("itemId") Long itemId) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpDelete request = new HttpDelete(apiPath+"/api/items/"+itemId);
            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", token);
            HttpResponse response = httpClient.execute(request);
            return ResponseEntity.status(response.getStatusLine().getStatusCode()).build();
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // CREATE NEW ITEM
    @Operation(summary = "Create new item", tags = "Bazar items", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Item successfully added"),
            @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(hidden = true)), description = "Bad request, item is not defined correctly"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")

    })
    @PostMapping(path = "/items", produces = "application/json")
    public ResponseEntity createItem(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token, @RequestBody Item item) {
        item.userId = loginServiceController.getUserId(token);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost(apiPath+"/api/items");
            StringEntity params = new StringEntity(new Gson().toJson(item));
            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", token);
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // GET ALL ITEMS WITH SPECIFIED CATEGORY
    @Operation(summary = "Get all items with given category", tags = "Bazar items", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Successfully retrieved"),
            @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(hidden = true)), description = "Bad request: Invalid argument value for order by parameter"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")

    })
    @GetMapping(path = "/items/category/{categoryId}", produces = "application/json")
    public ResponseEntity getItemsWithCategory(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token, @PathVariable Long categoryId) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpGet request = new HttpGet(apiPath+"/api/items?category="+categoryId);
            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", token);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // GET MY ITEMS WITH SPECIFIED CATEGORY
    @Operation(summary = "Get user items with given category", tags = "Bazar items", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Successfully retrieved"),
            @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(hidden = true)), description = "Bad request: Invalid argument value for order by parameter"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")

    })
    @GetMapping(path = "/my-items/category/{categoryId}", produces = "application/json")
    public ResponseEntity getMyItemsWithCategory(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token, @PathVariable Long categoryId) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            Long userId = loginServiceController.getUserId(token);
            HttpGet request = new HttpGet(apiPath+"/api/items?category="+categoryId+"&user%20id="+userId);
            request.addHeader("content-type", "application/json");
            request.addHeader("Authorization", token);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }
}
