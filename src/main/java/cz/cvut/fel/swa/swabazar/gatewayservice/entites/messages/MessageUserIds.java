package cz.cvut.fel.swa.swabazar.gatewayservice.entites.messages;

import io.swagger.v3.oas.annotations.media.Schema;

public class MessageUserIds {
    @Schema(example= "2", description = "Sender user id ")
    public Long fromUserId;
    @Schema(example= "8", description = "Receiver user id ")
    public Long toUserId;
}
