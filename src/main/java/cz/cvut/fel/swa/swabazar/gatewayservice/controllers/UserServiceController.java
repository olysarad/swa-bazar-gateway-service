package cz.cvut.fel.swa.swabazar.gatewayservice.controllers;

import com.google.gson.Gson;
import cz.cvut.fel.swa.swabazar.gatewayservice.entites.messages.Message;
import cz.cvut.fel.swa.swabazar.gatewayservice.entites.user.User;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.*;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(path = "/v1/user")
@SecurityScheme(
        name = "bearerAuth",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class UserServiceController {
    @Autowired
    LoginServiceController loginServiceController;

    @Value("${user.service.path}")
    private String apiPath;

    // GET MY USER
    @Operation(summary = "Get user", tags = "User", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Success"),
            @ApiResponse(responseCode = "401", content = @Content(schema = @Schema(hidden = true)), description = "Unauthorized - token is not valid"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")
    })
    @GetMapping(path = "", produces = "application/json")
    public ResponseEntity getMyUser(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token) {
        Long userId = loginServiceController.getUserId(token);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpGet request = new HttpGet(apiPath+"/api/v1/user/"+userId);
            request.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // UPDATE USER
    @Operation(summary = "Update user", tags = "User", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "User has been updated"),
            @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(hidden = true)), description = "Request body has invalid format"),
            @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(hidden = true)), description = "User not found"),
            @ApiResponse(responseCode = "422", content = @Content(schema = @Schema(hidden = true)), description = "Targeted mail or username already present in database"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")
    })
    @PutMapping(path = "", produces = "application/json")
    public ResponseEntity updateMyUser(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token, @RequestBody User user) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPut request = new HttpPut(apiPath+"/api/v1/user");
            StringEntity params = new StringEntity(new Gson().toJson(user));
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            return ResponseEntity.status(response.getStatusLine().getStatusCode())
                    .contentType(MediaType.valueOf(entity.getContentType().getValue()))
                    .body(EntityUtils.toString(entity, "UTF-8"));
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }

    // DELETE USER
    @Operation(summary = "Delete user", tags = "User", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(hidden = true)), description = "Success"),
            @ApiResponse(responseCode = "401", content = @Content(schema = @Schema(hidden = true)), description = "Unauthorized - token does not exist"),
            @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(hidden = true)), description = "Internal server error")
    })
    @DeleteMapping(path = "", produces = "application/json")
    public ResponseEntity deleteMyUser(@RequestHeader(value = "Authorization", defaultValue = "Bearer <token>") String token) {
        Long userId = loginServiceController.getUserId(token);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpDelete request = new HttpDelete(apiPath+"/api/v1/user/"+userId);
            request.addHeader("content-type", "application/json");
            HttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                return ResponseEntity.status(response.getStatusLine().getStatusCode()).build();
            }
            return loginServiceController.deleteUserFromLogin(token);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        }
    }
}
