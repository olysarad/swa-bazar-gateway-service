# SWA Bazar Gateway service

### How to run
```
git clone git@gitlab.fel.cvut.cz:olysarad/swa-bazar-gateway-service.git
cd swa-bazaar-gateway-service
docker-compose up --build
```
##### How to stop running container
```
docker-compose down
```


### Swagger documentation
http://localhost:8089/swagger-ui/index.html


### Docker Hub
https://hub.docker.com/repository/docker/olysarad/swa-gateway-service
