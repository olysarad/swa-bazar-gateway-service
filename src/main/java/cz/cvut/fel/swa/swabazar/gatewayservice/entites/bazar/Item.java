package cz.cvut.fel.swa.swabazar.gatewayservice.entites.bazar;

public class Item {
    public Category category;
    public String description;
    public String name;
    public Long price;
    public Long userId;
}
